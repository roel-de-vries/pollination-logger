using CsvHelper.Configuration;

namespace PollinationLogger.Models
{
    public sealed class PollinationLogRecordCsvMap : ClassMap<PollinationLogRecord>
    {
        public PollinationLogRecordCsvMap()
        {
            Map(plr => plr.PopulationId);
            Map(plr => plr.PlantId);
            Map(plr => plr.LogDate).TypeConverterOption.Format("dd/MM/yyyy");
            Map(plr => plr.ClusterId);
            Map(plr => plr.ClusterColor);
            Map(plr => plr.Flowermarker);
            Map(plr => plr.PollenAmount);
            Map(plr => plr.FlowerColor);
            Map(plr => plr.PollinatedBefore);
            Map(plr => plr.FlowermarkerBefore);
            Map(plr => plr.Remarks);
            Map(plr => plr.FlowerColor);
        }
    }
}