using System;
using System.ComponentModel.DataAnnotations;

namespace PollinationLogger.Models
{
    public class PollinationLogRecord
    {
        [Required(ErrorMessage = "Pollination ID is required")]
        public string PopulationId { get; set; }
        
        [Required(ErrorMessage = "Plant ID is required")]
        public string PlantId { get; set; }
        
        [Required(ErrorMessage = "Logging date is required")]
        public DateTime LogDate { get; set; }
        
        [Required(ErrorMessage = "Cluster ID is required")]
        public string ClusterId { get; set; }
        
        [Required(ErrorMessage = "Cluster color is required")]
        public string ClusterColor { get; set; }
        
        [Required(ErrorMessage = "Flowermarker is required")]
        public string Flowermarker { get; set; }
        
        public PollenAmount PollenAmount { get; set; }
        
        public FlowerColor FlowerColor { get; set; }
        
        public bool PollinatedBefore { get; set; }
        
        public string FlowermarkerBefore { get; set; }
        
        public string Remarks { get; set; }
    }
}