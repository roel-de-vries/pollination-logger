import { get, set } from 'https://unpkg.com/idb-keyval@5.0.2/dist/esm/index.js';

let file;
let fileHandle;

export async function initLoad() {
    fileHandle = await get('file');
    
    if(!fileHandle) {
        return;
    }

    if(!await verifyPermission(fileHandle)) {
        return;
    }
    
    await getFile(fileHandle);
}

export async function handleFileSelect() {
    try {
        fileHandle = (await window.showOpenFilePicker())[0];
    } catch {
        return false;
    }

    if(!fileHandle) {
        return false;
    }
    
    if(!fileHandle.name.endsWith(".csv")) {
        alert("Selected file isn't a CSV file. All other files are unsupported.");
        return false;
    }

    if(!await verifyPermission(fileHandle)) {
        return false;
    }

    if(!await getFile(fileHandle)){
        return false;
    }
    
    await set('file', fileHandle);
    return true;
}

export async function loadFile() {
    if(!file) {
        return null;
    }
    
    try {
        return await file.text();
    }
    catch {
        alert("Looks like the doesn't exist anymore. Please re-select it.");
        return null;
    }
}

export async function writeToFile(input) {
    try {
        const writable = await fileHandle.createWritable();
        await writable.write(input);
        await writable.close();
    }
    catch(ex) {
        alert("Something went wrong during the write to the file. Make sure that the file still exists and that it isn't being used by an other process.");
        console.log(ex);
    }
}

async function getFile(fileHandle) {
    try {
        file = await fileHandle.getFile();
        return true;
    }
    catch {
        alert("Looks like the doesn't exist anymore. Please re-select it.");
        return false;
    }
}

async function verifyPermission(fileHandle) {
    const options = {
        mode: "readwrite"
    };
    
    // Check if permission was already granted. If so, return true.
    if ((await fileHandle.queryPermission(options)) === 'granted') {
        return true;
    }
    // Request permission. If the user grants permission, return true.
    if ((await fileHandle.requestPermission(options)) === 'granted') {
        return true;
    }

    alert("Application requires permission to filesystem in order to write to the file!");
    // The user didn't grant permission, so return false.
    return false;
}